const fs = require('fs');
const path = require('path');
const readFrom = path.join(__dirname, './lipsum.txt');

function problem() {
    fs.readFile(readFrom, 'utf-8', function (error, data) { //reading the given file
        if (error) {
            console.log('error while reading file: ' + error);
        } else {
            console.log("data has been read from " + readFrom);
            // adding to new file after converting it to UpperCase
            fs.writeFile('./toUpperCase.txt', data.toUpperCase(), function (error) {
                if (error) {
                    console.log('error while writing after converting to uppercase: ' + error);
                }
                else {
                    console.log('data has been added to new file after uppercase conversion');
                   // storing the name of the file to which we just wrote into filesnames.txt
                    fs.appendFile('./filenames.txt', 'toUpperCase.txt\n', (error) => {
                        if (error) {
                            console.error(error);
                        } else {

                            // 3) reading the new file, converting into lower case, spliting the sentenses and writing to another file
                            fs.readFile('./toUpperCase.txt', 'utf-8', function (error, uppercaseData) {
                                if (error) {
                                    console.log('error while reading upper case data: ' + error);
                                } else {
                                    const lowerCaseData = uppercaseData.toLowerCase();
                                    const splitSentences = lowerCaseData.split('. ');

                                    // writing the split sentences to a new file
                                    fs.writeFile('./splitContent.txt', JSON.stringify(splitSentences), function (err) {
                                        if (err) {
                                            console.log('error while writing split content: ' + err);
                                        }
                                        else {
                                            console.log('successfuly written split content into new file');
                                            // storing the name into filenames.txt
                                            fs.appendFile('./filenames.txt', 'splitContent.txt\n', function (error) {
                                                if (error) {
                                                    console.log('error while appending file name: ' + error);
                                                } else {
                                                    // 4) reading the new file, sorting it's content, writing the sorted content to a new file & storing it's name to filenames.txt
                                                    fs.readFile('./splitContent.txt', 'utf-8', function (error, splitContent) {
                                                        if (error) {
                                                            console.log('error while reading split contents: ' + error);
                                                        } else {
                                                            const sortedContent = JSON.parse(splitContent).sort();

                                                            // writing the sorted data into a new file, storing its name into filenames.txt
                                                            fs.writeFile('./sorted.txt', JSON.stringify(sortedContent), function (error) {
                                                                if (error) {
                                                                    console.log('error while writing the sorted data: ' + error);
                                                                } else {
                                                                    // storing the name into filenames.txt
                                                                    fs.appendFile('./filenames.txt', 'sorted.txt\n', function (error) {
                                                                        if (error) {
                                                                            console.log('error while appending the file name: ' + error);
                                                                        } else {
                                                                            // 5) reading all the file names from filenmaes.txt and deleting them simultaneously
                                                                            fs.readFile('./filenames.txt', 'utf-8', function (error, fileNames) {
                                                                                if (error) {
                                                                                    console.log('error while reading file names: ' + error);

                                                                                } else {
                                                                                    // we are having an empty line in the filenames.txt because of '\n' whith the last name
                                                                                    fileNmaesArr = fileNames.split('\n').filter(Boolean);
                                                                                    // deleting the previously created files
                                                                                    fileNmaesArr.map((fileName) => {
                                                                                        fs.unlink(fileName, function (error) {
                                                                                            if (error) {
                                                                                                console.log('error while deleting the files: ' + error);
                                                                                            }
                                                                                            else {
                                                                                                console.log("Deleted " + fileName);
                                                                                            }
                                                                                        });
                                                                                    });
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }

                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }

                    });
                }
            });
        }
    });

}

module.exports = problem;