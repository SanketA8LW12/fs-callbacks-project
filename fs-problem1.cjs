const fs = require('fs');
const path = require('path');

function mkdir(dirPath, numberOfFiles) {
    fs.mkdir(dirPath, (error) => {
        if (error) {
            console.log(error);
        }
        else {
            console.log("Directory Created");
            createFiles(dirPath, numberOfFiles);
        }
    })
};


function createFiles(dirPath, numberOfFiles) {
    let count = 0;
    let pathArr = [];

    for(let index = 0; index < numberOfFiles; index++){
        let fileName = `${index + 1}.json`;

        fs.writeFile(`${dirPath}/${fileName}`, "File is created", (error) => {
            if (error) {
            console.log(error);
            }
            else {
                count++;
                console.log("File Created");
                pathArr.push(fileName);
                //deleteFiles(filePath);
            }

            if(count == numberOfFiles){
                callBack(error, pathArr);
            }
        });
    }
    function callBack(error, data){
        if(error){
            console.log(error);
        }
        else{
            console.log("Files created carefully");
            deleteFiles(pathArr, dirPath);
        }
    }
}


function deleteFiles(pathArr, dirPath) {
    let count = 0;
    for(let index = 0; index < pathArr.length; index++){
        fs.unlink(`${dirPath}/${pathArr[index]}`, (error) =>{
            if(error){
                console.log(error);
            }
            else{
                count++;
                console.log("File Deleted");
            }
    
            if(count == pathArr.length){
                callBack(error, `${"Files deleted successfully"}`);
            }
        })
    }
    function callBack(error, data){
        if(error){
            console.log(error);
        }
        else{
            console.log(data);
        }
      }
}

module.exports = mkdir;